// Marian Hristov - 2033348
package LinearAlgebra;

public class Vector3d {
    private double x;
    private double y;
    private double z;

    public Vector3d(double x, double y, double z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public double getX() {
        return this.x;
    }

    public double getY() {
        return this.y;
    }

    public double getZ() {
        return this.z;
    }

    public double magnitude() {
        double magnitude = Math
                .sqrt((this.getX() * this.getX()) + (this.getY() * this.getY()) + (this.getZ() * this.getZ()));
        return magnitude;
    }

    public double dotProduct(Vector3d vector) {
        double dot = (this.getX() + vector.getX()) + (this.getY() + vector.getY()) + (this.getZ() + vector.getZ());
        return dot;
    }

    public Vector3d add(Vector3d secondVector) {
        Vector3d newVector = new Vector3d(this.getX() + secondVector.getX(), this.getY() + secondVector.getY(),
                this.getZ() + secondVector.getZ());
        return newVector;
    }
}
