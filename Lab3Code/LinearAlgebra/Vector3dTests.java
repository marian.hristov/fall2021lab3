// Marian Hristov - 2033348
package LinearAlgebra;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.Test;

public class Vector3dTests {

    @Test
    public void testConstructorGetX() {
        Vector3d vector = new Vector3d(1, 2, 3);
        assertEquals(vector.getX(), 1);
    }

    @Test
    public void testConstructorGetY() {
        Vector3d vector = new Vector3d(1, 2, 3);
        assertEquals(vector.getY(), 2);
    }

    @Test
    public void testConstructorGetZ() {
        Vector3d vector = new Vector3d(1, 2, 3);
        assertEquals(vector.getZ(), 3);
    }

    @Test
    public void testMagnitude() {
        Vector3d vector = new Vector3d(1, 2, 3);
        assertEquals(vector.magnitude(), 3.7416573867739413);
    }

    @Test
    public void testDotProduct() {
        Vector3d vectorOne = new Vector3d(1, 2, 3);
        Vector3d vectorTwo = new Vector3d(4, 5, 6);
        assertEquals(vectorOne.dotProduct(vectorTwo), 21);
    }

    @Test
    public void testAdd() {
        Vector3d vectorOne = new Vector3d(1, 2, 3);
        Vector3d vectorTwo = new Vector3d(4, 5, 6);
        Vector3d vectorThree = vectorOne.add(vectorTwo);
        assertEquals(vectorThree.getX(), 5);
        assertEquals(vectorThree.getY(), 7);
        assertEquals(vectorThree.getZ(), 9);
    }
}
